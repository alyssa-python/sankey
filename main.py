import os
import requests
from collections import namedtuple
import plotly.graph_objects as go
from pathlib import Path

BASE_URL = f"https://api.airtable.com/v0/{os.getenv('AIRTABLE_BASE_CASHFLOW')}"

headers = { 
    "Authorization": f"Bearer {os.getenv('AIRTABLE_API_KEY')}",
    # "Content-Type": "application/json"
}

# Make an API call and store the response.

# filterByFormula={Status}="Done", see https://codepen.io/airtable/full/rLKkYB?baseId=appHJBdRL96s2xZTU&tableId=tbl22yoXC2MPFaxUn
url = f"{BASE_URL}/Sankey 2021?maxRecords=1000&filterByFormula=%7BStatus%7D%3D%22Done%22"

r = requests.get(url, headers=headers)
# print(f"Status code: {r.status_code}")

inputs = []
Input = namedtuple('Input', 'source, value, target')

def process_results(dict):
    for transaction in dict['records']:
        fields = transaction['fields']
        if 'Category' in fields.keys():
            inputs.append(Input(fields['Source'], fields['Amount'], fields['Category']))
            inputs.append(Input(fields['Category'], fields['Amount'], fields['Target']))
        else:
            inputs.append(Input(fields['Source'], fields['Amount'], fields['Target']))

while True:
    # Store API response in a variable.
    response_dict = r.json()

    # print(f"Got {len(response_dict['records'])} records(s)\n")
    process_results(response_dict)
    offset = response_dict['offset'] if 'offset' in response_dict.keys() else None

    if(offset):
        # print(offset)
        r = requests.get(f"{url}&offset={offset}", headers=headers)
    else:
        break

# print(*inputs)
# print(*inputs, sep='\n')
# print(*(f"{source} [{value}] {target}" for (source, value, target) in inputs), sep='\n')

# Output for http://sankeymatic.com/build/
# print(*(f"{i.source} [{i.value}] {i.target}" for i in inputs), sep='\n')

labels = list(dict.fromkeys([i.source for i in inputs] + [i.target for i in inputs]))

# print(*[i.source for i in inputs], sep='\n')
# print(*labels, sep='\n')
# print([labels.index(i.source) for i in inputs])
# print([i.target for i in inputs])

fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = labels,
      # TODO generate array of rgba()
      color = "blue",

    #   customdata = ["Long name A1", "Long name A2", "Long name B1", "Long name B2",
    #                 "Long name C1", "Long name C2"],

      # https://plotly.com/python/reference/sankey/ %{fullData.name}
    #   hovertemplate='Node has total value %{value}<extra>%{fullData.selectedpoints}</extra>',
    ),
    link = dict(
      source = [labels.index(i.source) for i in inputs],
      target = [labels.index(i.target) for i in inputs],
      value =  [i.value for i in inputs],
      # Custom Data
    #   collaborator = []
    #   status = []  

    #   customdata = ["q","r","s","t","u","v"],
    #   hovertemplate='Link from node %{source.customdata}<br />'+
    #     'to node%{target.customdata}<br />has value %{value}'+
    #     '<br />and data %{customdata}<extra></extra>',
  ))])

Path("./dist").mkdir(parents=True, exist_ok=True)
fig.write_html('./dist/sankey.html', auto_open=True)

# fig.update_layout(title_text="2021 Cash Flow", font_size=10)
# fig.show()

# TODO flow cross-check (might need to be in JavaScript, not Python)